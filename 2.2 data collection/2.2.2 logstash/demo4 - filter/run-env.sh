#!/bin/bash

docker run -p 8080:8080 -it \
	-v "`pwd`":/app \
	-v "`pwd`/logstash.conf":/usr/share/logstash/pipeline/logstash.conf \
	docker.elastic.co/logstash/logstash-oss:6.0.0 bash
