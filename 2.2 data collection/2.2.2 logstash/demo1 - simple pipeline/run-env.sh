#!/bin/bash

docker run -p 8080:8080 -it \
	-v "`pwd`":/app \
	docker.elastic.co/logstash/logstash-oss:6.0.0 bash
