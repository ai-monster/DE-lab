#!/bin/bash

rm -rf /usr/share/logstash/config/*
rm -rf /usr/share/logstash/pipeline/*
logstash -e 'input { stdin {} } output { stdout {} }'
