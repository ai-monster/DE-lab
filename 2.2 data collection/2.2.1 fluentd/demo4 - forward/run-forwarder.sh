#!/bin/bash
docker run -d --name mygateway -p 24224:24224 \
	-v "`pwd`":/app fluent/fluentd fluentd -c /app/forward.conf
