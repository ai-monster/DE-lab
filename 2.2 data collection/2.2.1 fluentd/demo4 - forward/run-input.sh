#!/bin/bash
docker run -it -p 9880:9880 --link mygateway:mygateway \
	-v "`pwd`":/app fluent/fluentd fluentd -c /app/input.conf
