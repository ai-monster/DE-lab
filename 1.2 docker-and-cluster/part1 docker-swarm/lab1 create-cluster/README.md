# Create Swarm Server

## 開啟Swarm mode

在您想要設定的swarm master主機上，執行init指令

```
$ docker swarm init
```

下面是執行結果的回覆...

```
Swarm initialized: current node (td5wd9hz3awxyu9uyco8lu621) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0cluuigquq001z1sipqh183b7immcleurlkha8la4xsx6pot0s-aa01j4dgdmu1zq3wsyxdwbadv 192.168.65.2:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

## 讓Slave主機加入

到您想要設定為Slave的主機上執行下面指令：

```
docker swarm join --token SWMTKN-1-0cluuigquq001z1sipqh183b7immcleurlkha8la4xsx6pot0s-aa01j4dgdmu1zq3wsyxdwbadv 192.168.65.2:2377
```

## Swarm Service

### 建立swarm service

透過service create可以建立指定的service，建議加上--name來命名您的service...

```
instance-1 # docker service create --name web nginx
49naaqey2ai2r8wusrrjuulmm
overall progress: 1 out of 1 tasks
1/1: running   [==================================================>]
verify: Service converged
```

建立完成後可以透過service ls來檢視是否完成

```
instance-1 # docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
49naaqey2ai2        web                 replicated          1/1                 nginx:latest
```

### Scale out您的Service

在service運作的當下，我們可以透過scale這個指令來擴展我們的service replica...

```
instance-1 # docker service scale web=3
web scaled to 3
overall progress: 3 out of 3 tasks
1/3: running   [==================================================>]
2/3: running   [==================================================>]
3/3: running   [==================================================>]
verify: Service converged
instance-1 # docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
49naaqey2ai2        web                 replicated          3/3                 nginx:latest
```

如果要縮減回來，一樣使用scale，指令數量變小即可...

### 檢視service log

有需要的話，我們也可以使用 logs -f 來監看service的運作狀況...

```
instance-1 # docker service logs -f web
```

### 移除service

當service不在需要時，可以透過rm刪除service，清除資源...

```
docker service rm web
```

