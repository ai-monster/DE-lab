# Run Wordpress 

## Using Single Node

Single Node方便您可以在單機中測試，執行方式如下：

```
docker-compose -f wordpress-singlenode.yaml
```

## Using Compose

本專案中已經準備好wordpress swarm mode compose檔案(wordpress-swarm.yaml)，該檔案是以docker compose建制，但由於有一個以上的container，其中會使用到network的設定，需要先enable docker swarm mode，然後再以docker stack來deploy該compose服務。

### 建立wordpress

啟動wordpress在docker stack模式...

```
$ docker stack deploy -c wordpress-swarm.yaml wp
```

啟動後可以透過ps檢視

```
$ docker stack ps wp
ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE           ERROR               PORTS
kqroniqvqr1w        wp_db.1             mysql:5.7           moby                Running             Running 8 minutes ago
b8szztk4wnp2        wp_wordpress.1      wordpress:latest    moby                Running             Running 8 minutes ago
```

有需要檢視log可以搭配docker service來執行... 首先確認docker stack啟動了多少的docker service...

```
$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
illljj6f3nnt        wp_db               replicated          1/1                 mysql:5.7
05sgyem7p6yz        wp_wordpress        replicated          1/1                 wordpress:latest    *:8000->80/tcp
```

然後可以透過docker service logs來檢視service的log...

```
$ docker service logs -f wp_wordpress
wp_wordpress.1.b8szztk4wnp2@moby    | WordPress not found in /var/www/html - copying now...
wp_wordpress.1.b8szztk4wnp2@moby    | Complete! WordPress has been successfully copied to /var/www/html
wp_wordpress.1.b8szztk4wnp2@moby    |
wp_wordpress.1.b8szztk4wnp2@moby    | Warning: mysqli::mysqli(): php_network_getaddresses: getaddrinfo failed: Name or service not known in - on line 22
wp_wordpress.1.b8szztk4wnp2@moby    |
wp_wordpress.1.b8szztk4wnp2@moby    | Warning: mysqli::mysqli(): (HY000/2002): php_network_getaddresses: getaddrinfo failed: Name or service not known in - on line 22
...(skip)
```

### Scale Wordpress資源

將wordpress擴充至兩台

```
$ docker service scale wp_wordpress=2
```

將wordpress改成一台

```
$ docker service scale wp_wordpress=1
```


### 清除資源

```
$ docker stack rm wp
```
