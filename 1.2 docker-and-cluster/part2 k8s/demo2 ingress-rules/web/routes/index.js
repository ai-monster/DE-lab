var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.all('/foo*', function(req, res) {
  res.send('foo....');
});

router.all('/bar*', function(req, res) {
  res.send('bar....');
});
module.exports = router;
