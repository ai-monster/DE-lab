# Wordpress at K8S

本範例參考k8s官方[範例](https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/)，並稍做修改，修改部分如下：
* 將disk改小，避免佔據太多畚箕空間
* 將service的type改為NodePort，以使用minikube本機作為服務

## 建立PV

```
kubectl create -f local-volumes.yaml
```

## 建立 secret

```
kubectl create secret generic mysql-pass --from-literal=password=1234qwer
```

## 建立MySQL

```
kubectl create -f mysql-deployment.yaml
```

## 建立Wordpress

```
kubectl create -f wordpress-deployment.yaml
```

檢視服務：

```
minikube service wordpress
```
