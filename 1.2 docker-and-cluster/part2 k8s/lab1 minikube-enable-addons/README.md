# 啟用minikube addons

## 安裝minikube

macOS只需要透過brew即可快速安裝... 

```
brew cask install minikube
```

Linux環境可以直接下載執行檔，放到環境變數可以吃到的路徑即可...

```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && \
chmod +x minikube && \
sudo mv minikube /usr/local/bin/
```

Windows的下載網址如下：
https://storage.googleapis.com/minikube/releases/latest/minikube-windows-amd64.exe 

如果您的kubectl尚未安裝，可以直接使用google cloud sdk來安裝：

```
curl https://sdk.cloud.google.com | bash
gcloud components install kubectl
```

安裝完成後，原則上minikube會在本地端加入minikube的k8s context，我們可以透過下面指令來使用該context…

```
kubectl config use-context minikube
```

然後，可檢查一下您的minikube node是否正常運作....

```
$ kubectl get node -o wide
NAME       STATUS    AGE       VERSION   EXTERNAL-IP   OS-IMAGE            KERNEL-VERSION
minikube   Ready     2d        v1.7.5    <none>        Buildroot 2017.02   4.9.13
```

## 啟動與關閉minikube

畢竟minikube會實際使用主機的資源，如果在不需要操作時候，可以透過stop將minikube關閉，當然，也可以start他...

```
$ minikube stop
Stopping local Kubernetes cluster...
Machine stopped.
$ minikube start
Starting local Kubernetes v1.7.5 cluster...
Starting VM...
Getting VM IP address...
Moving files into cluster...
Setting up certs...
Connecting to cluster...
Setting up kubeconfig...
Starting cluster components...
Kubectl is now configured to use the cluster.
```

檢視minikube實際ip位置
minikube另外提供一個更簡單的指令來顯示ip位置，未來可以透過這個指令來結合一些自動化程式...

```
$ minikube ip
192.168.99.100
```


## 開啟minikube的擴充套件
minikube提供addons來擴充minikube，我們可以使用minikube addons list來查看哪些addons可以使用... 然後把他enable或是disable...

### 開啟ingress服務
上面所介紹的簡單操作，最後minikube service nginx是以k8s service的部分顯示nginx可以連線的位置，由於minikube為單機服務，因此也沒有很需要使用真正的service(真正的service將會有特定的主機擔任traffic forwarding的任務，在開通的過程中，會需要通知該主機把port對應上所要導向的pod… 而在雲服務，service則會與layer 4 load balancer結合...)。而ingress因為為layer 7層的網路服務，其中包含load balancer的特殊功能，因此minikube特地把ingress放在addons list中，讓使用者可以快速的啟用他... 下面是檢視目前addons與啟用ingress的方式...

```
# minikube addons list
- addon-manager: enabled
- kube-dns: enabled
- heapster: enabled
- registry-creds: disabled
- dashboard: enabled
- default-storageclass: enabled
- ingress: enabled
- registry: disabled

# minikube addon enable ingress
```

啟用後，我們可以再次檢視是否已經完成啟用...


### 開啟heapster服務
heapster是kubernetes中負責蒐集k8s內container運作的相關資訊的服務，他通常會結合Grafana來做BI的查詢... 同ingress的啟用方式，我們也可以快速的將heapster addon啟動...

```
$ minikube addon enable heapster
```

接下來可以開啟heapster的UI… 這邊會直接開啟Grafana作為客製化報表查詢的地方...

```
$ minikube addon open heapster
```

如果想要知道heapster的連線資訊，也可以直接用minikube service來show出url位置，但因為heapster是屬於kube-system這個namespace，所以這邊就需要多指定namespace為kube-system…

```
$ minikube service monitoring-grafana -n kube-system --url
http://192.168.99.100:31644
```


