# Parameter Issue

Run a mysql instance...

```
docker run -d -p 3306:3306 mysql
478ada946e049b3707774d74fa6c0744a29d75f30ff8cd8f0559da9a7e5d7fb6
```

Check with process... but not found the running mysql instance...

```
# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

Check the log...

```
# docker logs -f 478ada946
error: database is uninitialized and password option is not specified
  You need to specify one of MYSQL_ROOT_PASSWORD, MYSQL_ALLOW_EMPTY_PASSWORD and MYSQL_RANDOM_ROOT_PASSWORD
```

