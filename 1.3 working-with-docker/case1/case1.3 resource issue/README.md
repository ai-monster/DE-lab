# Resource Issue

Here is a case for port conflict...

```
bash -x run1.sh
```

Then...

```
bash -x run2.sh
```

You will got a resource conflict result...

```
# bash -x run2.sh
+ docker run -d -p 3000:3000 peihsinsu/simpleweb
4c0ca11d5116ebe74fcd1a2d5201c8a91aff9ffb21ff26ce52f82cf32ea7e70b
docker: Error response from daemon: driver failed programming external connectivity on endpoint angry_tesla (c8cfcf8e4d51b13267792ff4a85080a5209507fcad316b4ec4e3bce0a02209bb): Bind for 0.0.0.0:3000 failed: port is already allocated.
```

## Solve...

You should use other port for runnig your container. Or you can use proxy way to orchistration your container.


