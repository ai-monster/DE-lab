# Case 1.1 Docker image issue

Build the dockerfile

```
docker build -t test/web .
```

Run

```
docker run -d -p 3000:3000 test/web
```

Debug

```
docker logs -f [id]
```

Try to run the command inside container...

```
docker run -it test/web bash
```

Run the command in the container...

```
node app.js
```

You will got empty return when execute, because the web default start should use...

```
npm start
```

Fix the Dockerfile and build again....

```
From node

ADD ./web /app

WORKDIR /app

RUN npm install 

CMD ["npm","start"]
```
