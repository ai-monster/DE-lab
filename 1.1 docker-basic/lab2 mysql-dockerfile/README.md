# MySQL Dockerfile

## Create a mysql dockerfile

Add create database script

dump.sql:
```
create database mydb
```

Dockerfile:
```
From mysql

ADD dump.sql /docker-entrypoint-initdb.d/
```

## Build and run to check the database will create when image run

Build the mysql container using image name: test/mysql...

```
docker build -t test/mysql .
```

Run the mysql container you build...

```
docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234qwer test/mysql
```

Check is the database: mydb created...

```
mysql -uroot -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.7.20 MySQL Community Server (GPL)

Copyright (c) 2000, 2017, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mydb               |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.01 sec)

mysql>
```
