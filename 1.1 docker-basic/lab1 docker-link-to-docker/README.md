# Lab1 - Docker link to Docker

## Create a docker network

```
docker network create mynet
```

check...

```
docker network list
```

## Create 2 mysql docker instance

c1 as mysql server
```
docker run -d --name=c1 -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234qwer mysql
```

c2 link to c1 as mysql client
```
docker run --name=c2 -it --link c1:c1 mysql bash
```

## Try using c2 to connect c1 to connect mysql server in c1

```
docker exec -it c2 bash
```

after connect to c2..., connect using mysql command...
```
mysql -hc1 -uroot -p1234qwer
```

