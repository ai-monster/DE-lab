# AI Monster 數據工程課程

## Environment Prepare

相關需求：

* git
* docker

### Install docker

```
curl -sSL https://get.docker.com | sh
```

其他環境請參考：https://docs.docker.com/install/

### Install docker-compose

```
sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

其他環境請參考：https://docs.docker.com/compose/install/

## Pull project

```
git clone https://gitlab.com/ai-monster/DE-lab.git
```
