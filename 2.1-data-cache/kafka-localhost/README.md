# Examples

Start Zookeeper

```
./bin/zookeeper-server-start.sh config/zookeeper.properties
```

Start Kafka

```
./bin/kafka-server-start.sh config/server.properties
```

Create topic

```
./bin/kafka-topics.sh \
  --create \
  --zookeeper localhost:2181 \
  --replication-factor 1 \
  --partitions 1 \
  --topic test
```

Start producer

```
./bin/kafka-console-producer.sh \
  --broker-list localhost:9092 \
  --topic test
```

Start consumer

```
./bin/kafka-console-consumer.sh \
  --bootstrap-server localhost:9092 \
  --topic test \
  --from-beginning
```


